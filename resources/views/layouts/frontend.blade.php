<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>BLOG</title>
    <link rel="icon" href="images/favicon.ico">

    @include('frontend.statics.css')

    @section('pushcss')
        <!-- EXTRA CCS -->
    @show('pushcss')
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="90">
    <!--Loader Start-->
        @include('frontend.loader')
    <!--Loader End-->

    <!--Header Start-->
        <header class="cursor-light">
            @include('frontend.header')
        </header>
    <!--Header end-->

    <!--slider-->
        <section id="home" class="cursor-light p-0">
            @include('frontend.slider')
        </section>
    <!--slider end-->
        @yield('content')
    <!--Footer Start-->
        @include('frontend.footer')
    <!--Footer End-->

    <!--Scroll Top-->
    <a class="scroll-top-arrow" href="javascript:void(0);"><i class="fa fa-angle-up"></i></a>
    <!--Scroll Top End-->

    <!--Animated Cursor-->
    <div id="aimated-cursor">
        <div id="cursor">
            <div id="cursor-loader"></div>
        </div>
    </div>

    @include('frontend.statics.js')
    @section('pushjs')
        <!-- EXTRA JS -->
    @show('pushjs')
</body>
</html>
