<!--Navigation-->
<nav class="navbar navbar-top-default navbar-expand-lg navbar-gradient nav-icon">
    <div class="container">
        <a href="javascript:void(0)" title="Logo" class="logo link scroll">
            <!--Logo Default-->
            <img src="{{ asset('frontend/images/logo-white.png') }}" alt="logo" class="logo-dark default">
        </a>

        <!--Nav Links-->
        <div class="collapse navbar-collapse" id="wexim">
            <div class="navbar-nav ml-auto">
                    <a class="nav-link link scroll" href="#home">Home</a>
                    <a class="nav-link link scroll" href="#about">About</a>
                    <a class="nav-link link scroll" href="#team">Team</a>
                    <a class="nav-link link scroll" href="#portfolio">Work</a>
                    <a class="nav-link link scroll" href="#price">Pricing</a>
                    <a class="nav-link link scroll" href="#blog">Blog</a>
                    <a class="nav-link link scroll" href="#contact">Contact</a>
                <span class="menu-line"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
            </div>
        </div>

        <!--Side Menu Button-->
        <a href="javascript:void(0)" class="d-inline-block parallax-btn sidemenu_btn" id="sidemenu_toggle">
            <div class="animated-wrap sidemenu_btn_inner">
            <div class="animated-element">
                <span></span>
                <span></span>
                <span></span>
            </div>
            </div>
        </a>

    </div>
</nav>

<!--Side Nav-->
<div class="side-menu">
    <div class="inner-wrapper">
        <span class="btn-close link" id="btn_sideNavClose"></span>
        <nav class="side-nav w-100">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link link scroll" href="#home">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link scroll" href="#about">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link scroll" href="#team">Team</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link scroll" href="#portfolio">Work</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link scroll" href="#price">Pricing</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link scroll" href="#blog">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link scroll" href="#contact">Contact</a>
                </li>
            </ul>
        </nav>

        <div class="side-footer text-white w-100">
            <ul class="social-icons-simple">
                <li class="animated-wrap"><a class="animated-element" href="javascript:void(0)"><i class="fa fa-facebook"></i> </a> </li>
                <li class="animated-wrap"><a class="animated-element" href="javascript:void(0)"><i class="fa fa-instagram"></i> </a> </li>
                <li class="animated-wrap"><a class="animated-element" href="javascript:void(0)"><i class="fa fa-twitter"></i> </a> </li>
            </ul>
            <p class="text-white">&copy; 2019 Wexim. Made With Love by Themesindustry</p>
        </div>
    </div>
</div>
<a id="close_side_menu" href="javascript:void(0);"></a>
<!-- End side menu -->
