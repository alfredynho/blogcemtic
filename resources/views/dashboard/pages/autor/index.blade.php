@extends('layouts.dashboard') // Esta heredando de la plantilla Padre

@section('extracss')

@endsection('extracss')

@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Listado de Registros de autores {{ $autores }}</h3>
            </div>

            <div class="card-body">
                <div>
                    <td>
                        <a href="{{ route('autor.create') }}">
                            <button type="button" class="btn btn-block btn-outline-primary btn-xs">CREAR NUEVO REGISTRO</button>
                        </a>
                    </td>
                </div>

              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Llave Primaria</th>
                  <th>Nombre</th>
                  <th>Fecha Registro</th>
                  <th>Modificar</th>
                  <th>Eliminar</th>
                </tr>
                </thead>
                <tbody>
                @foreach($autores as $autor)
                    <tr>
                        <td>{{ $autor->id }}</td>
                        <td>{{ $autor->nombre }}</td>
                        <td>{{ $autor->created_at }}</td>
                        <td>
                            <a href="{{ route('autor.show',$autor->id) }}">
                                <button type="button" class="btn btn-block btn-outline-success btn-xs">MODIFICAR</button>
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('autor.delete',$autor->id) }}">
                                <button type="button" class="btn btn-block btn-outline-danger btn-xs">ELIMINAR</button>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>

              </table>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>
@endsection

@section('extrajs')

@endsection('extrajs')
