@extends('layouts.dashboard')

@section('extracss')

@endsection('extracss')

@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Listado de Registros</h3>
            </div>

            <div class="card-body">
                <div>
                    <td>
                        <a href="{{ route('autor.create') }}">
                            <button type="button" class="btn btn-block btn-outline-primary btn-xs">Primary</button>
                        </a>
                    </td>
                </div>

              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
                </tr>
                </thead>
                <tbody>
                @foreach($autores as $autor)
                    <tr>
                        <td>{{ $autor->id }}</td>
                        <td>{{ $autor->nombre }}</td>
                        <td>-</td>
                        <td>-</td>
                        <td>U</td>
                    </tr>
                @endforeach

                </tbody>

              </table>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>
@endsection

@section('extrajs')

@endsection('extrajs')
