<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

// Listado para Autores en index
Route::get('/', 'Frontend\CemticController@index')->name('index');

Route::get('/admin', 'Dashboard\AutorController@dashboard')->name('dashboard');

// rutas de autor
Route::get('/listar-autores', 'Dashboard\AutorController@index')->name('lista.autor'); // VISTO

Route::get('/crear', 'Dashboard\AutorController@create')->name('autor.create'); //


Route::post('/guardar', 'Dashboard\AutorController@store')->name('autor.store');
Route::get('/delete/{id}', 'Dashboard\AutorController@destroy')->name('autor.delete'); // BORRAR REGISTRO
Route::put('/update/{id}', 'Dashboard\AutorController@update')->name('autor.update');
Route::get('/show/{id}', 'Dashboard\AutorController@show')->name('autor.show');

// rutas de Blog
Route::get('/lista-blog', 'Dashboard\BlogController@index')->name('autor.index');




























Route::get('/clear/{cod}',function($cod){
    if($cod=='Cemtic.2020+'){
        Artisan::call('cache:clear');
        Artisan::call('config:clear');
        Artisan::call('route:clear');
        Artisan::call('view:clear');
        return '<h1> caches borrados Exitosamente - CEMTIC </h1>';
    }else{
        return back();
    }
});
