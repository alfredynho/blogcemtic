<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Autor;
use DB;
use Session;

class AutorController extends Controller
{
   public function dashboard(Request $request){
    // DB::table('orders')
        $autores = Autor::all(); // select *from autor;

        return view('admin',
            ['autores' => $autores],
    );
    // animo1
    // animo2
    // animo3
    // $autores = DB::table('autor');

    // dd("Tipo de Variable >> ".gettype($autores));
    // dd("Autores Consola >> ".$autores);

    // foreach($autores as $autor){
    //     $autor->id;
    //     $autor->nombre; // Hello, {{ $nombre }}.
    // }
   }

   // Listar
   // Modificar
   // Eliminar
   // Crear

   public function index(Request $request)
   {
       // Funcion para Listar Autores
       $autores = Autor::orderBy('id','desc')->get();

       return view('dashboard.pages.autor.index', [
           'autores' => $autores,
       ]);
   }

   public function store(Request $request)
   {
        // dump($request);
       // Funcion para guardar Autores
       $autor = new Autor();
       $autor->nombre = $request->get('nombre');

       $autor->save(); // Guardarlo en DB

    //    Session::flash('create','Registro creado con Éxito');
       return Redirect()->route('lista.autor');
   }

   public function update(Request $request, $id)
   {
       // Function para modificar
       $autor = Autor::findOrFail($id);
       $autor->nombre = $request->input('nombre');
       $autor->update();

       return Redirect()->route('lista.autor');
   }

   public function show($id)
   {
       // Funcion para visualizar el registro
       $autor = Autor::where('id', '=', $id)->firstOrFail();
       return view('dashboard.pages.autor.show', [
           'autor' => $autor
       ]);
   }

   public function create(Request $request)
   {
       // Funcion para mostrar el formulario
       $message = "Crear Autor";
       return view('dashboard.pages.autor.create', [
           'message' => $message
       ]);
   }

   public function destroy($id)
   {
       // Funcion para eliminar el registro
       $autor = Autor::findOrFail($id); // Buscando al autor para poder eliminarlo
    //    Session::flash('delete','Registro '.$autor->nombre.' eliminado con Éxito');
       $autor->delete();

       return Redirect()->route('lista.autor');
   }
}
