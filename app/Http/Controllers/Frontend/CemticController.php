<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Autor;

class CemticController extends Controller
{
    public function index(Request $request){
        $autores = Autor::all(); // select *from autor;
        return view('index',
            ['autores' => $autores],
        );

    }
}
